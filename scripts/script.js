"use strict";
// 1. Для пошуку спеціальних символів [ ] \ ^ $ . | ? * + ( ), нам треба додати перед ними \ («екранувати їх»).
// 2. Виклик методу — Method Invocation.
//     Виклик функції — Function Invocation.
//     Виклик конструктору — Constructor Invocation.
//     Виклик apply та call — Apply And Call Invocation.
// 3. Hoisting, тобто спливання, підняття, це механізм, при якому змінні та оголошення функції 
//     піднімаються вгору по своїй області видимості перед виконанням коду.

function createNewUser() {
    this.firstName = prompt("Enter your firstname");
    this.lastName = prompt("Enter your lastname");
    this.birthday = prompt ('Enter your birthday', 'dd.mm.yyyy');
    this.getLogin = function() {
        return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    }
    this.getAge = function() {
        return 2022 - this.birthday.slice(6);
    }
    this.getPassword = function() {
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
    }
}
let newUser = new createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());